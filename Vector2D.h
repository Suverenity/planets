#ifndef VECTOR2D_H
#define VECTOR2D_H


class Vector2D
{
public:
	double x, y;
	Vector2D(double x, double y );
	Vector2D();
	Vector2D(const Vector2D& other);
	double computeMetric();
	double computeDistanceOfTwoVectors(const Vector2D& other);
	void reset();

	Vector2D operator+ (const Vector2D& rhs);
	Vector2D operator- (const Vector2D& rhs);
	Vector2D operator* (const double rhs);
};

#endif // VECTOR2D_H
