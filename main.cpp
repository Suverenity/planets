#include "MainWindow.h"
#include "Vector2D.h"
#include <QApplication>

#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <iterator>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w;
	w.show();
//	w.createProblem();
//	w.addPlanet(Planet({0,0}, {100,100},100), Qt::blue);
//	w.addPlanet(Planet({0,0}, {100,-100},100), Qt::red);
//	w.addPlanet(Planet({0,0}, {0,10},100), Qt::yellow);
	w.start();

	return a.exec();
}
