#include "ComputingCoordinator.h"
#include "MainWindow.h"
#include "Planet.h"
#include <thread>
#include <chrono>
#include <math.h>
#include <Vector2D.h>

void ComputingCoordinator::compute(Vector2D& value)
{
	if(!UorT){
		computeU(value);
	} else {
		computeT(value);
	}
}

void ComputingCoordinator::setFrame(Vector2D frame)
{
	this->frame = frame;
}

void ComputingCoordinator::computeU(Vector2D& position)
{
	position.reset();
	auto currentPlanet = planets.at(computedPlanet)->pl;
	for(size_t i=0; i < planets.size(); i++){
		if(i == computedPlanet){
			continue;
		}
		auto planet = planets.at(i);
		auto planetsDistance = currentPlanet.getPosition().computeDistanceOfTwoVectors(planet->pl.getPosition()) + 10e-5;
		position.x += G * currentPlanet.getMass() * planet->pl.getMass() * ( planet->pl.getPosition().x - currentPlanet.getPosition().x) / (pow(planetsDistance, 1.5));
		position.y += G * currentPlanet.getMass() * planet->pl.getMass() * ( planet->pl.getPosition().y - currentPlanet.getPosition().y) / (pow(planetsDistance, 1.5));
	}
}

void ComputingCoordinator::computeT(Vector2D &momentum)
{
	momentum.reset();
	momentum.x = planets.at(computedPlanet)->pl.getMomentum().x / planets.at(computedPlanet)->pl.getMass();
	momentum.y = planets.at(computedPlanet)->pl.getMomentum().y / planets.at(computedPlanet)->pl.getMass();
}

void ComputingCoordinator::checkBoundaries()
{
	for(auto& planet: planets){
		auto &momentum = planet->pl.getMomentum();
		auto &position = planet->pl.getPosition();
		if(position.x <= PLANET_SIZE || position.x >= (frame.x-PLANET_SIZE)){
			momentum.x = -momentum.x;
			if(position.x <= PLANET_SIZE) position.x = PLANET_SIZE + 1;
			if(position.x >= frame.x-PLANET_SIZE) position.x = frame.x-PLANET_SIZE-1;
		}
		if(position.y <= PLANET_SIZE || position.y >= (frame.y-PLANET_SIZE) ){
			momentum.y = -momentum.y;
			if(position.y <= PLANET_SIZE) position.y = PLANET_SIZE + 1;
			if(position.y >= frame.x-PLANET_SIZE) position.y = frame.y-PLANET_SIZE-1;
		}
	}
}

ComputingCoordinator::ComputingCoordinator(std::vector<std::shared_ptr<ColoredPlanet> >& planets, std::function<void()> doneCallback, std::function<void()> stoppedCallback)
	: planets(planets)
	, solver(*this)
	, updateCallback(doneCallback)
	, stoppedCallback(stoppedCallback)
	, pausedForComputing (false)
	, running (false)
	, pausedOuter (true)
	, computedPlanet(0)
	, G(1)
	, UorT(true)
	, draw(true)
{ }

ComputingCoordinator::~ComputingCoordinator()
{ }

void ComputingCoordinator::run()
{
	size_t ignoredUpdates = 0;
	solver.setStep(0.025);
	this->running = true;
	while(running){

		if(pausedForComputing || pausedOuter){
			std::this_thread::sleep_until(std::chrono::system_clock::now() + std::chrono::milliseconds(1));
			continue;
		}

		for (auto & planet: planets){
			planet->pl.setPositionTmp(solver.solve(planet->pl.getPosition()));
			UorT = false;
			planet->pl.setMomentumTmp(solver.solve(planet->pl.getMomentum()));
			UorT = true;
			computedPlanet ++;
		}
		computedPlanet = 0;
		for (auto& planet: planets){
			planet->pl.switchValues();
		}

		if(++ignoredUpdates == 3){
			if(draw){
				pause();
				updateCallback();
			}
			ignoredUpdates = 0;
		}
	}
	stoppedCallback();
}

void ComputingCoordinator::stop()
{
	running = false;
}

void ComputingCoordinator::pause(bool value)
{
	pausedForComputing = value;
}

void ComputingCoordinator::pauseOuter(bool val)
{
	pausedOuter = val;
}

void ComputingCoordinator::setDraw(bool val)
{
	draw = val;
}
