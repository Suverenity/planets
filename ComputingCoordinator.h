#ifndef COMPUTINGCOORDINATOR_H
#define COMPUTINGCOORDINATOR_H

#include "RungeKuttaSolver.h"
#include "Vector2D.h"

#include <vector>
#include <memory>
#include <functional>

struct ColoredPlanet;
class Vector2D;

class ComputingCoordinator
{
	std::vector<std::shared_ptr<ColoredPlanet>>& planets;
	RungeKuttaSolver solver;
	std::function<void()> updateCallback;
	std::function<void()> stoppedCallback;
	bool pausedForComputing, running, pausedOuter;
	size_t computedPlanet;
	double G;
	Vector2D frame;
	bool UorT; //true = U; false = T
	bool draw;

	void computeU(Vector2D &position);
	void computeT(Vector2D &momentum);
	void checkBoundaries();
public:
	ComputingCoordinator(std::vector<std::shared_ptr<ColoredPlanet> > &planets,
						 std::function<void()> updateCallback,
						 std::function<void()> stoppedCallback);
	~ComputingCoordinator ();
	void run();
	void compute(Vector2D &value);
	void setFrame(Vector2D frame);

//	callback
public:
	void stop();
	void pause(bool value = true);
	void pauseOuter(bool val);
	void setDraw(bool val);
};

#endif // COMPUTINGCOORDINATOR_H
