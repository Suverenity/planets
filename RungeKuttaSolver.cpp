#include "RungeKuttaSolver.h"
#include "ComputingCoordinator.h"
#include "Vector2D.h"

RungeKuttaSolver::RungeKuttaSolver(ComputingCoordinator &problem)
	: problem(problem)
{}

void RungeKuttaSolver::setStep(double step)
{
	this->step = step;
}

Vector2D RungeKuttaSolver::solve(Vector2D& initVal)
{
	Vector2D k1(initVal);
	problem.compute(k1);
	Vector2D k2;
	k2 = initVal + (k1*step*0.5f);
	problem.compute(k2);
	Vector2D k3;
	k3 = initVal + k2*step*0.5f;
	problem.compute(k3);
	Vector2D k4;
	k4 = initVal + k3*step;
	problem.compute(k4);
	Vector2D res;
	res = initVal + ((k1 + k2*2.0f + k3*2.0f + k4) * step) *0.16666f;
	return res;
}
