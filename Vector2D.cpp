#include "Vector2D.h"
#include <math.h>

Vector2D::Vector2D(double x, double y)
	: x(x), y(y)
{ }

Vector2D::Vector2D()
	: x(0.0f), y(0.0f)
{ }

Vector2D::Vector2D(const Vector2D &other)
{
	this->x =  other.x;
	this->y = other.y;
}

double Vector2D::computeMetric()
{
	return sqrt(x*x + y*y);
}

double Vector2D::computeDistanceOfTwoVectors(const Vector2D &other)
{
	Vector2D res(x, y);
	res = res - other;
	return res.computeMetric();
}

void Vector2D::reset()
{
	x = 0.0f;
	y = 0.0f;
}

Vector2D Vector2D::operator+(const Vector2D &rhs)
{
	Vector2D res;
	res.x = rhs.x + x;
	res.y = rhs.y + y;
	return res;
}

Vector2D Vector2D::operator-(const Vector2D &rhs)
{
	Vector2D res;
	res.x = x - rhs.x ;
	res.y = y - rhs.y ;
	return res;
}

Vector2D Vector2D::operator*(const double rhs)
{
	Vector2D res;
	res.x = this->x * rhs;
	res.y = this->y * rhs;
	return res;
}
