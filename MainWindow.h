#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ComputingCoordinator.h"
#include "Planet.h"

#include <QMainWindow>
#include <memory>

#define DRAWING_STEPS_IGNORED 1
#define NUMBER_OF_HISTORY 10000

#define RNG_MASS_LIMIT 1000
#define RNG_VELOCITY_MIN_LIMIT -5000
#define RNG_VELOCITY_MAXLIMIT 5000
#define RNG_NUMBER_OF_PLANETS 9
#define WHEEL_ZOOM_STEP 0.5

namespace Ui {
class MainWindow;
}

class Vector2D;

struct ColoredPlanet {
	Planet pl;
	QColor color;
	std::vector<Vector2D> history;
	ColoredPlanet(Planet pl, QColor color)
		:pl(pl), color(color)
	{}
};

class MainWindow : public QMainWindow
{
	Q_OBJECT
	Ui::MainWindow *ui;
	std::vector<std::shared_ptr<ColoredPlanet> > planets;
	ComputingCoordinator coordinator;
	Vector2D offsetWindow;
	Vector2D offsetClicked;
	Vector2D currentTopLeftCorner;
	Vector2D middleOfView;
	double zoom;
	size_t currentPointIgnored;
	bool drawHistory;

	bool innerLoopStopped;

	void changeColor();
	void init();
	double generateDouble(double min, double max);
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	void addPlanet(Planet pl,  QColor color);
	void generateRandomPlanet();
	void start();
	void createProblem();

protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	void mousePressEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent * event) Q_DECL_OVERRIDE;
private slots:
	void on_pauseCB_toggled(bool checked);
	void on_tracesCB_toggled(bool checked);
	void on_createProblemPB_clicked(bool checked);
	void on_drawCB_toggled(bool checked);
};

#endif // MAINWINDOW_H
