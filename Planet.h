#ifndef PLANET_H
#define PLANET_H

#include <vector>
#include <functional>
#include "Vector2D.h"

#define NUMBER_OF_EQS 6
#define PLANET_SIZE 2


class Planet
{
	Vector2D momentum, position;
	Vector2D momentumTmp, positionTmp;
	double mass;
public:
	Planet();
	Planet(Vector2D position, Vector2D velocity, double mass);

	Vector2D& getPosition();
	Vector2D& getMomentum();
	void setMomentumTmp(Vector2D val);
	void setPositionTmp(Vector2D val);
	void switchValues();
	double getMass();
};

#endif // PLANET_H
