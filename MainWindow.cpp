#include "MainWindow.h"
#include "Vector2D.h"
#include "ui_MainWindow.h"
#include <QPainter>
#include <thread>
#include <iostream>
#include <QMouseEvent>
#include <iostream>
#include <time.h>
#include <cstdio>
#include <QMutex>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, coordinator(planets,
				  [this](){this->update();},
				  [this](){this->innerLoopStopped = true;})
	, middleOfView(0.0,0.0)
	, zoom(10.0)
	, currentPointIgnored(0)
	, innerLoopStopped(false)
	, drawHistory(true)

{
	srand(time(NULL));
	ui->setupUi(this);
	this->init();
	currentTopLeftCorner.x = -this->width()/2;
	currentTopLeftCorner.y = this->height()/2;
}

MainWindow::~MainWindow()
{
	coordinator.stop();
	while(!innerLoopStopped){ }
	delete ui;
}

void MainWindow::addPlanet(Planet pl,  QColor color)
{
	auto v = std::make_shared<ColoredPlanet>(pl, color);
	planets.push_back(v);
	this->update();
}

void MainWindow::generateRandomPlanet()
{
	Planet p1({rand()%this->width()*zoom - this->width()/2*zoom, rand()%this->height()*zoom - this->width()/2*zoom},
			  {generateDouble(RNG_VELOCITY_MIN_LIMIT, RNG_VELOCITY_MAXLIMIT),generateDouble(RNG_VELOCITY_MIN_LIMIT, RNG_VELOCITY_MAXLIMIT)},
			  (rand()%RNG_MASS_LIMIT));
	addPlanet(p1, QColor(rand()%255, rand()&255, rand()%255));
}

void MainWindow::start()
{
	std::thread t(&ComputingCoordinator::run, &coordinator);
	t.detach();
}

void MainWindow::createProblem()
{
//	size_t planets = rand() %RNG_NUMBER_OF_PLANETS + 3;
	addPlanet(Planet({0,0}, {0,0},PLANET_SIZE*1000000), Qt::yellow);
	for (size_t i = 0; i < RNG_NUMBER_OF_PLANETS-1; ++i) {
		generateRandomPlanet();
	}
}

void MainWindow::changeColor()
{
	QPalette pal;
	pal.setColor(QPalette::Background, Qt::black);
	this->setPalette(pal);
}

void MainWindow::init()
{
	this->changeColor();
	coordinator.setFrame({static_cast<double>(this->width()),static_cast<double>(this->height()) });
}

double MainWindow::generateDouble(double min, double max)
{
	std::mt19937 rng;
	std::uniform_real_distribution<double> dist(min, max);
	rng.seed(std::random_device{}());
	return dist(rng);
}

void MainWindow::paintEvent(QPaintEvent * event)
{
	QMainWindow::paintEvent(event);
	QPainter painter(this);
	offsetWindow.x = this->width()/2;
	offsetWindow.y = this->height()/2;

	if(drawHistory){
		for (auto &planet : planets){
			painter.setPen(planet->color);
			painter.setBrush(planet->color);
				for(auto point: planet->history){
						painter.drawPoint(((point.x + middleOfView.x) / zoom + offsetWindow.x),
										  ((-point.y + middleOfView.y) / zoom + offsetWindow.y));
				}

		}
	}

	if( currentPointIgnored == DRAWING_STEPS_IGNORED-1)
	{
//		#pragma omp parallel for
//		for(auto &planet : planets){
		for (size_t i = 0; i < planets.size(); i++){
			planets[i]->history.push_back(Vector2D(planets[i]->pl.getPosition()));
			if(planets[i]->history.size() >= NUMBER_OF_HISTORY){
				planets[i]->history.erase(planets[i]->history.begin());
			}
		}
	}

	for (auto &planet : planets){
		painter.setPen(planet->color);
		painter.setBrush(planet->color);
		painter.drawEllipse(QPoint( (planet->pl.getPosition().x + middleOfView.x) / zoom + offsetWindow.x,
									(-planet->pl.getPosition().y + middleOfView.y) / zoom + offsetWindow.y),
									static_cast<size_t>(PLANET_SIZE + (PLANET_SIZE*planet->pl.getMass()/100)/(zoom/10)),
									static_cast<size_t>(PLANET_SIZE + (PLANET_SIZE*planet->pl.getMass()/100)/(zoom/10)));
	}

	currentPointIgnored = (currentPointIgnored + 1)%DRAWING_STEPS_IGNORED;
	coordinator.pause(false);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
	middleOfView.x = (event->pos().x() - offsetClicked.x) * zoom  + middleOfView.x ;
	middleOfView.y = (event->pos().y() - offsetClicked.y) * zoom  + middleOfView.y ;
	update();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
	offsetClicked.x = event->pos().x();
	offsetClicked.y = event->pos().y();
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
	QMutex lock;
	lock.lock();
	middleOfView.x = (this->size().width()/2 - event->pos().x())*zoom + middleOfView.x;
	middleOfView.y = (this->size().height()/2 - event->pos().y())*zoom + middleOfView.y;
	if(event->delta() > 0){
		zoom *=WHEEL_ZOOM_STEP;
	}else {
		zoom /=WHEEL_ZOOM_STEP;
	}
	lock.unlock();
	update();
}

void MainWindow::on_pauseCB_toggled(bool checked)
{
	coordinator.pauseOuter(checked);
}

void MainWindow::on_tracesCB_toggled(bool checked)
{
	drawHistory = checked;
}

void MainWindow::on_createProblemPB_clicked(bool checked)
{
	createProblem();
}

void MainWindow::on_drawCB_toggled(bool checked)
{
	coordinator.setDraw(checked);
}
