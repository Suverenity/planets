#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T16:19:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Planets
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11 -fopenmp
QMAKE_LFLAGS +=  -fopenmp

linux{
	QMAKE_CXXFLAGS += -pthread
}

mac {
	QMAKE_CXXFLAGS += -stdlib=libc++
}

SOURCES += main.cpp\
	MainWindow.cpp \
    RungeKuttaSolver.cpp \
    Planet.cpp \
    Vector2D.cpp \
    ComputingCoordinator.cpp

HEADERS  += MainWindow.h \
    RungeKuttaSolver.h \
    Planet.h \
    Vector2D.h \
    ComputingCoordinator.h

FORMS    += MainWindow.ui
