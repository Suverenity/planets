#include "Planet.h"

Vector2D &Planet::getPosition()
{
	return position;
}

Vector2D &Planet::getMomentum()
{
	return momentum;
}

void Planet::setMomentumTmp(Vector2D val)
{
	momentumTmp = val;
}

void Planet::setPositionTmp(Vector2D val)
{
	positionTmp = val;
}

void Planet::switchValues()
{
	position = positionTmp;
	momentum = momentumTmp;
}

double Planet::getMass()
{
	return mass;
}

Planet::Planet()
{ }

Planet::Planet(Vector2D position, Vector2D velocity, double mass)
	: position(position)
	, mass (mass)
{
	momentum.x = velocity.x * mass;
	momentum.y = velocity.y * mass;
}
