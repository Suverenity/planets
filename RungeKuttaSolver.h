#ifndef RUNGEKUTTASOLVER_H
#define RUNGEKUTTASOLVER_H

#include <memory>

class ComputingCoordinator;
class Vector2D;

class RungeKuttaSolver
{
	double step;
	ComputingCoordinator& problem;
public:
	RungeKuttaSolver(ComputingCoordinator& problem);
	void setStep(double step);
	Vector2D solve(Vector2D &initVal);
};

#endif // RUNGEKUTTASOLVER_H
